/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author gugle
 */
public class Message implements Serializable{
    private String nick, ip, texto,auxiliar;

    public String getAuxiliar() {
        return auxiliar;
    }

    public void setAuxiliar(String auxiliar) {
        this.auxiliar = auxiliar;
    }
    
    private ArrayList<String>direcciones;
    
    private ArrayList<String>prueba;
    
    private HashMap<String, String>todo;
    
    private int sesion;

    public int getSesion() {
        return sesion;
    }

    public void setSesion(int sesion) {
        this.sesion = sesion;
    }
    

    public HashMap<String, String> getTodo() {
        return todo;
    }

    public void setTodo(HashMap<String, String> todo) {
        this.todo = todo;
    }
    

    public ArrayList<String> getPrueba() {
        return prueba;
    }

    public void setPrueba(ArrayList<String> prueba) {
        this.prueba = prueba;
    }
   
    public ArrayList<String> getDirecciones() {
        return direcciones;
    }

    public void setDirecciones(ArrayList<String> direcciones) {
        this.direcciones = direcciones;
    }

    public Message(String nick, String ip, String texto) {
        this.nick = nick;
        this.ip = ip;
        this.texto = texto;
    }

    public Message() {
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}
