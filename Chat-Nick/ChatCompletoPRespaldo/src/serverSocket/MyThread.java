/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverSocket;

import client.Message;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import views.ClientInterface;
import views.ServerInterface;

/**
 *
 * @author gugle
 */
public class MyThread extends Thread {

    private ServerInterface serverInterface;

    private ClientInterface clienteInterface;

    private String nombreFinal;

    private int n = 0;

    public String getNombreFinal() {
        return nombreFinal;
    }

    public void setNombreFinal(String nombreFinal) {
        this.nombreFinal = nombreFinal;
    }

    private Prueba prueb;

    public MyThread(String name, ServerInterface serverInterfaz) {
        super(name);
        this.serverInterface = serverInterfaz;
        start();
    }

    @Override
    public void run() {
        ServerSocket serverSocket;
        Socket server;
        try {
            serverSocket = new ServerSocket(9999);
            //System.out.println("Ya creó el ServerSocket");
            Message receivedMessage;

            ArrayList<String> ipes = new ArrayList<>();
            ArrayList<String> pru = new ArrayList<>();
            HashMap<String, String> informacion = new HashMap<>();
            while (true) {
                try {
                    server = serverSocket.accept();
                    ObjectInputStream objectInputStream = new ObjectInputStream(server.getInputStream());
                    try {
                        receivedMessage = (Message) objectInputStream.readObject();
                        if (!receivedMessage.getTexto().equals(" Conexion")) {
                            serverInterface.getMenssages().append("\nNick: " + receivedMessage.getNick() + " - Ip: "
                                    + receivedMessage.getIp() + " - Texto: " + receivedMessage.getTexto());
                            Socket enviarD = new Socket(receivedMessage.getIp(), 9090);
                            ObjectOutputStream reenvio = new ObjectOutputStream(enviarD.getOutputStream());
                            reenvio.writeObject(receivedMessage);
                            reenvio.close();
                            enviarD.close();
                        } else {

                            if (n == 0) {
                                /// Codigo Online
                                InetAddress direccion = server.getInetAddress();
                                String ipremota = direccion.getHostAddress();
                                ipes.add(ipremota);
                                int permiso = 0;

                                receivedMessage.setDirecciones(ipes);
                                pru.add(receivedMessage.getNick());
                                receivedMessage.setPrueba(pru);
                                String nomb = receivedMessage.getNick();
                                
                                int tamanotes=informacion.size();
                                
                                informacion.put(ipremota, nomb);
                                receivedMessage.setNick(receivedMessage.getAuxiliar());                                
                                int sa = informacion.size();
                                
                                
                                
                                int entrar = 0;
                                if (sa == tamanotes) {
                                   entrar++;
                                }
                                if (entrar == 0) {

                                    receivedMessage.setTodo(informacion);

                                    for (String i : ipes) {
                                        Socket enviarD = new Socket(i, 9090);
                                        ObjectOutputStream reenvio = new ObjectOutputStream(enviarD.getOutputStream());
                                        reenvio.writeObject(receivedMessage);
                                        reenvio.close();
                                        enviarD.close();
                                    }
                                }
                            }//
                            else {

                            }
                        }
                        //Informacion reenviada
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(MyThread.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    //serverInterface.getMenssages().setText(serverInterface.getMenssages().getText() + "\n" + inputStream.readUTF());

                    server.close();
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

}
