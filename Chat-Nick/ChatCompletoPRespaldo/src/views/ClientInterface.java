/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

/**
 *
 * @author gugle
 */
import client.Message;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.*;
import serverSocket.Prueba;
import serverSocket.Ventana;
public class ClientInterface extends JFrame implements Serializable, Runnable {
    private final JLabel lblIp, lblMessage, lblMessages;
    private final JTextField txtMessage, txtmasivo;
    private final JComboBox txtIp;
    public JLabel txtClient;
    private final JLabel nombre;
    HashMap<String, String> informacion = new HashMap<>();
    public JLabel getTxtClient() {
        return txtClient;
    }
    private final JTextArea txtMessages;
    private final JButton btnSubmit, btnLimpiar;
    private final JPanel panel;
    private Prueba pr = new Prueba();
    private Socket socketClient;

    public ClientInterface(String title) {
        lblIp = new JLabel("Clientes: ");
        lblMessage = new JLabel("Mensaje: ");
        lblMessages = new JLabel("-----Chat Masivo:-----");
        txtmasivo = new JTextField(25);
        txtMessage = new JTextField(25);
        txtMessages = new JTextArea(10, 25);
        String cliente = JOptionPane.showInputDialog("Ingrese nombre: ");
        nombre = new JLabel("Nombre: ");
        txtClient = new JLabel();
        txtClient.setText(cliente);
        //  Message message = new Message(txtClient.getText(),"", "");       
        pr.setNombre(txtClient.getText());
        txtIp = new JComboBox();
        btnSubmit = new JButton("Enviar");
        btnLimpiar = new JButton("Enviar M");

        // Message message = new Message(cliente,"","");
        btnSubmit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    //System.out.println("Hola funciona");
                    socketClient = new Socket("192.168.1.70", 9999);
                    try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(socketClient.getOutputStream())) {
                        
                        //Obtener nombre        
                        String dato = txtIp.getSelectedItem().toString();
                        int indice=txtIp.getSelectedIndex();
                        
                        int x=0;
                        for (Map.Entry entry : informacion.entrySet()) {
                                    if (x == indice) {
                                        // String name = entry.getValue().toString();
                                        Object items = entry.getKey();
                                        dato=items.toString();
                                        //txtIp.addItem(name);
                                    }
                                    x++;
                                }                        
                        Message message = new Message(txtClient.getText(),
                                dato, txtMessage.getText());
                        txtMessages.append("\n Yo:" + txtMessage.getText());
                        objectOutputStream.writeObject(message);      
                    }
                    socketClient.close();
                    txtMessage.setText("");
                } catch (IOException ex) {
                    txtMessages.setText(txtMessages.getText() + "\n" + txtMessage.getText() + " - Message not sent.");
                    System.out.println(ex.getMessage());
                }
            }
        });

        //CHAT MASIVO
        btnLimpiar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    //System.out.println("Hola funciona");

                    ArrayList<String> masivo = new ArrayList<>();
                    String global = txtmasivo.getText();
                    String variable = "";

                    for (int i = 0; i < global.length(); i++) {
                        char s = global.charAt(i);
                        if (i + 1 > global.length() - 1) {
                            variable = variable + s;
                            masivo.add(variable);
                            System.out.println("Nombres: " + variable);
                            variable = "";
                        } else if (s == ';') {
                            masivo.add(variable);
                            System.out.println("Nombres: " + variable);
                            variable = "";
                        } else {
                            variable = variable + s;
                        }
                    }

                    ArrayList<String> masivoipes = new ArrayList<>();

                    for (int i = 0; i < masivo.size(); i++) {

                        String nombre = masivo.get(i);

                        for (int j = 0; j < txtIp.getItemCount(); j++) {

                            String dato = txtIp.getItemAt(j).toString();

                            if (nombre.equals(dato)) {
                                //String info = txtIp.getItemAt(j).toString();;
                                // dato = String.valueOf(informacion.get(info));
                                // masivoipes.add(dato);
                                int n = 0;
                                for (Map.Entry entry : informacion.entrySet()) {
                                    if (n == j) {
                                        // String name = entry.getValue().toString();
                                        Object items = entry.getKey();
                                        masivoipes.add(items.toString());
                                        //txtIp.addItem(name);
                                    }
                                    n++;
                                }

                            }
                        }
                    }

                    for (int i = 0; i < masivoipes.size(); i++) {

                        socketClient = new Socket("192.168.1.70", 9999);
                        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(socketClient.getOutputStream())) {
                            //outputStream.writeUTF(txtMessage.getText());   

                            if (i == 0) {
                                Message message = new Message(txtClient.getText(),
                                        masivoipes.get(i), txtMessage.getText());
                                txtMessages.append("\n Yo:" + txtMessage.getText());
                                objectOutputStream.writeObject(message);
                            }else
                            {
                                Message message = new Message(txtClient.getText(),
                                        masivoipes.get(i), txtMessage.getText());
                                objectOutputStream.writeObject(message);
                            }
                        }
                        socketClient.close();
                    }
                    txtMessage.setText("");
                } catch (IOException ex) {
                    txtMessages.setText(txtMessages.getText() + "\n" + txtMessage.getText() + " - Message not sent.");
                    System.out.println(ex.getMessage());

                }
            }
        });

        panel = new JPanel();
        panel.add(nombre);
        panel.add(txtClient);

        panel.add(lblIp);
        panel.add(txtIp);
        panel.add(lblMessages);
        panel.add(txtmasivo);
        panel.add(txtMessages);
        panel.add(lblMessage);
        panel.add(txtMessage);
        panel.add(btnSubmit);
        panel.add(btnLimpiar);

        this.setBounds(800, 350, 320, 410);
        this.add(panel);
        this.setVisible(true);
        this.setTitle(title);

        addWindowListener(new Ventana());
        Thread hilo = new Thread(this);
        hilo.start();
    }

    @Override
    public void run() {
        ServerSocket serverSocket;
        Socket cliente;
        try {
            serverSocket = new ServerSocket(9090);
            //System.out.println("Ya creó el ServerSocket");
            Message receivedMessage;
            while (true) {
                try {
                    cliente = serverSocket.accept();
                    ObjectInputStream objectInputStream = new ObjectInputStream(cliente.getInputStream());
                    try {
                        receivedMessage = (Message) objectInputStream.readObject();
                        if (!receivedMessage.getTexto().equals(" Conexion")) {
                            txtMessages.append("\n" + receivedMessage.getNick() + ": " + receivedMessage.getTexto());
                        } else {
                            receivedMessage.setNick(receivedMessage.getNick());
                            txtIp.removeAllItems();
                            informacion = receivedMessage.getTodo();
                            for (Map.Entry entry : informacion.entrySet()) {
                                String name = entry.getValue().toString();
                                txtIp.addItem(name);
                            }
                        }
                    } catch (ClassNotFoundException ex) {
                        this.dispose();
                       // ex.getMessage();
                        
                    }
                } catch (IOException ex) {
                    this.dispose();
                  //  System.out.println(ex.getMessage());
                }
            }
        } catch (IOException ex) {
            this.dispose();
           // System.out.println(ex.getMessage());
        }

    }
}
