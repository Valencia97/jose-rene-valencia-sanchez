/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import javax.swing.JTextArea;


public class Hilo extends Thread {
    
    JTextArea txtMessages;
    public Hilo(String name,JTextArea ptxtMessages){
        super(name);
        start();
        txtMessages=ptxtMessages;
    }

    @Override
    public void run() {
        ServerSocket serverSocket;
        Socket server;
        try {
            serverSocket = new ServerSocket(9999);
            //System.out.println("Ya creó el ServerSocket");
            while (true)
            {
                try{
                    server = serverSocket.accept();
                    //System.out.println("Ya aceptó algo el servidor");
                    DataInputStream inputStream = new DataInputStream(server.getInputStream());
                    txtMessages.setText(txtMessages.getText() + "\n" + inputStream.readUTF());
                    System.out.println(inputStream.readUTF());
                    server.close();
                }
                catch (IOException ex){
                    System.out.println (ex.getMessage());
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }    
    
}
